#!/bin/bash

rm -rf ./k8s/docker/copy
mkdir ./k8s/docker/copy
cp .env ./k8s/docker/copy/
cp package.json ./k8s/docker/copy/
cp app.js ./k8s/docker/copy
cp -r public ./k8s/docker/copy/public
cp -r server ./k8s/docker/copy/server
cp -r views ./k8s/docker/copy/views

cd ./k8s/docker

TAG_NAME=$(docker images | grep getraenke -m 1 | grep  -oE '[0-9]*\.[0-9]+' | head -n 1 | cut -c 3-)
TAG_NAME=$(($TAG_NAME + 1))
echo Tag name: $TAG_NAME

docker build -t jonasgoetz01/jhgetraenke . --platform linux/amd64
docker tag jonasgoetz01/jhgetraenke jonasgoetz01/jhgetraenke:0.$TAG_NAME
docker push jonasgoetz01/jhgetraenke:0.$TAG_NAME
echo "Nach jonasgoetz01/jhgetraenke:0.$TAG_NAME gepusht"